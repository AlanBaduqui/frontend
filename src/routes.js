import React from 'react';
import { Route } from 'react-router-dom';

 import PeliculaList from './containers/PeliculaList';
 import PeliculaDetail from './containers/PeliculaDetail';

 const BaseRouter = ()=>(

    <div>
        <Route exact path='/' component={PeliculaList}/>
        <Route exact path='/:peliculaID' component={PeliculaDetail} />
    </div>

 );

 export default BaseRouter;