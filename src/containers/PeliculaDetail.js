import React from 'react';
import Axios from 'axios';
import { Card } from 'antd';

class PeliculaDetail extends React.Component{

    state= {
        titulo:[]
    }

    componentDidMount(){
        const peliculaID = this.props.match.params.peliculaID;
        Axios.get(`http://127.0.0.1:8000/api/${peliculaID}`)
        .then(res =>{
            this.setState({
                titulo: res.data
            });
            console.log(res.data);
        })
    }

    render(){
        return(
            <Card title={this.state.titulo.titulo}>
                <p>{this.state.titulo.resumen}</p>
            </Card>
        )
    }
}

export default PeliculaDetail;