import React from 'react';
import Peliculas from '../components/Pelicula';
import Axios from 'axios';

class PeliculaList extends React.Component{

    state= {
        titulos:[]
    }

    componentDidMount(){
        Axios.get('http://127.0.0.1:8000/api/')
        .then(res =>{
            this.setState({
                titulos: res.data
            });
        })
    }

    render(){
        return(
            <Peliculas data={this.state.titulos} />
        )
    }
}

export default PeliculaList;